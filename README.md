# Object File Converter

This utility can be used for converting object files between COFF/PE,
OMF, ELF and Mach-O formats for all 32-bit and 64-bit x86
platforms. Can modify symbol names in object files. Can build, modify
and convert function libraries across platforms. Can dump object files
and executable files. Also includes a very good disassembler
supporting the SSE4, AVX, AVX2, AVX512, FMA3, FMA4, XOP and Knights
Corner instruction sets.

Full documentation is available only in PDF format:
[objconv-instructions.pdf](./objconv-instructions.pdf).

This utility was written and is maintained by Agner Fog
<https://www.agner.org/>.  Official releases may be downloaded
from <https://www.agner.org/optimize/#objconv>.

This README documents an unofficial mirror of releases starting with
version 2.51, hosted at <https://bitbucket.org/elwoz/objconv> and
maintained by Zack Weinberg <https://www.owlfolio.org/>.  It exists
principally so that I can automatically build executables for MacOS
and Linux as well as Windows (a Windows executable is distributed with
the official releases), and secondarily because the official releases
are distributed in an inconvenient, nonstandard format.

The executables built from this mirror, along with a standard
GNU-style source tarball, may be downloaded from
<https://bitbucket.org/elwoz/objconv/downloads/>.

## Maintenance

This is a mirror, not a fork.  All bug reports and feature requests
regarding the actual code should be sent directly to Agner Fog.

However, bug reports and feature requests regarding the build
machinery added in this mirror (`configure.ac`, `Makefile.am`, and/or
`azure-pipelines.yml`) should be sent to Zack Weinberg by filing
issues on this repository.  If you happen to notice a new release
before I do, reporting that (again, by filing an issue) is also
welcome.

## Copyright and Licensing

All `.asm`, `.cpp`, and `.h` files, `build.sh`, `objconv.vcproj`, and
`objconv-instructions.pdf` are copyright 2006–2018 Agner Fog with
portions copyright Don Clugston.  These files are available under an
unspecified version of the GNU General Public License; for purposes of
redistribution by this mirror I am treating this as equivalent to the
conventional “version 2, or, at your option, any later version”
language.

This file, `azure-pipelines.yml`, `configure.ac`, and `Makefile.am`
are copyright 2020 Zack Weinberg.  Copying and distribution of these
files, with or without modification, are permitted in any medium
without royalty provided the copyright notice and this notice are
preserved.  These files are offered as-is, without any warranty.

COPYING contains the full text of the GNU General Public License,
version 2, except that the FSF’s old postal address and the “How to
Apply These Terms to Your New Programs” appendix have been deleted.
See <https://www.gnu.org/licenses/> for current contact information
for the FSF, and advice on use of the GPL.

Any files present in the source tarball, but not in the Git
repository, were created by either Autoconf or Automake.
They carry their own copyright and licensing statements.
